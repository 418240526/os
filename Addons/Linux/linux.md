# `Linux`背景

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [UNIX](#unix)
2. [Linux](#linux)
3. [OSS](#oss)
4. [Linux发行版主线](#linux发行版主线)

<!-- /code_chunk_output -->

## UNIX

1. 1968，`Multics`项目
1. 1970，`Unix`（`Assembly`）： “1970/01/01”是`Unix元年`，也是时间戳的起点
1. 1973，`Unix`（`C`）：
1. 1975，`BSD`：

## Linux

1. 1991，`v0.0.1`：`Linus's Unix`>>>`Linux`>>>`Linux Is Not Unix X`

## OSS

1. 1983，`GNU`
1. 1985，`FSF`
1. 1990，`Emacs`, `GCC`
1. 1991， `Linux`加入`GNU`
1. 1992， `GNU/Linux`

## Linux发行版主线

1. `Debian`
1. `RedHat`
1. `Slackware`/`SuSe`
